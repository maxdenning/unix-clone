#!/bin/sh
set -e

cd kernel
./clean.sh
./build.sh
cd ..

./iso.sh # . ./iso.sh

#qemu-system-$(./target-triplet-to-arch.sh $HOST) -cdrom myos.iso
qemu-system-i386 -cdrom myos.iso
