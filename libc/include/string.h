#ifndef _STRING_H
#define _STRING_H

#include <sys/cdefs.h>
#include <stddef.h>

#ifdef __cplusplus
extern "C" {
#endif


int memcmp(const void* lhs, const void* rhs, size_t count);
void* memcpy(void* __restrict dst, const void* __restrict src, size_t count);
void* memmove(void* dst, const void* src, size_t count);
void* memset(void* dst, int ch, size_t count);
size_t strlen(const char* str);


#ifdef __cplusplus
}
#endif

#endif
