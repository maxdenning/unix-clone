#ifndef _STDIO_H
#define _STDIO_H

#include <sys/cdefs.h>
#include "printf.h"

#define EOF (-1)

#ifdef __cplusplus
extern "C" {
#endif


int putchar(int ch);
int puts(const char* str);


#ifdef __cplusplus
}
#endif

#endif
