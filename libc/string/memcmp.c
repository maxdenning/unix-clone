#include <string.h>
#include <stdint.h>


int memcmp(const void* lhs, const void* rhs, size_t count)
{
    const uint8_t* l = (uint8_t*)lhs;
    const uint8_t* r = (uint8_t*)rhs;

    for(size_t i = 0U; i < count; i++)
    {
        if(l[i] < r[i])
        {
            return -1;
        }
        else if(r[i] < l[i])
        {
            return 1;
        }
    }
    return 0;
}
