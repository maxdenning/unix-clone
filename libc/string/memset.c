#include <string.h>
#include <stdint.h>

void* memset(void* dst, int ch, size_t count)
{
    uint8_t* const buf = (uint8_t*)dst;
    for(size_t i = 0U; i < count; i++)
    {
        buf[i] = (uint8_t)ch;
    }
    return dst;
}
