#include <string.h>
#include <stdint.h>

void* memcpy(void* __restrict dst, const void* __restrict src, size_t count)
{
    uint8_t* const d = (uint8_t*)dst;
    const uint8_t* const s = (uint8_t*)src;
    for(size_t i = 0U; i < count; i++)
    {
        d[i] = s[i];
    }
    return dst;
}
