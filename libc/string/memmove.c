#include <string.h>
#include <stdint.h>


void* memmove(void* dst, const void* src, size_t count)
{
    uint8_t* const d = (uint8_t*)(dst);
    const uint8_t* const s = (uint8_t*)(src);
    if(d < s)
    {
        for(size_t i = 0U; i < count; i++)
        {
            d[i] = s[i];
        }
    }
    else
    {
        for(size_t i = count; i != 0U; i--)
        {
            d[i - 1] = s[i - 1];
        }
    }
    return dst;
}
