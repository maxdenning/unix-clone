#include <stdio.h>
#include <stdlib.h>

__attribute__((__noreturn__))
void abort(void)
{
    #ifdef __is_libk
        //TODO: add proper kernel panic
        printf("kernel panic! abort()\n");
    #else
        //TODO: abnormally terminate the process as if by SIBABRT
        printf("abort()\n");
    #endif

    for(;;){}
    __builtin_unreachable();
}
