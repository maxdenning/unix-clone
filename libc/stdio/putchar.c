#include <stdio.h>

#if defined(__is_libk)
    #include "kernel/tty.h"
#endif


int putchar(int ch)
{
    #if defined(__is_libk)
        tty_write_c((char)ch);
    #else
        // TODO: Implement stdio and the write system call.
    #endif
	return ch;
}

// for compatability with sys/printf.h
void _putchar(char character)
{
    putchar(character);
}
