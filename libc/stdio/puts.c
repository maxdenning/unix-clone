#include <stdio.h>

#if defined(__is_libk)
    #include "kernel/tty.h"
#endif


int puts(const char* str)
{
    #if defined(__is_libk)
        tty_write_s(str);
        return 1;
    #else
        return printf("%s\n", str);
    #endif
}
