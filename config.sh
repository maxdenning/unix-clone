#SYSTEM_HEADER_PROJECTS="libc kernel"
#PROJECTS="libc kernel"

# target platform
export MAKE=${MAKE:-make}
export HOST=${HOST:-i686-elf}
export HOSTARCH=${HOSTARCH:-i386}

# platform specific tools
export AR=${HOST}-ar
export AS=${HOST}-as
export CC=${HOST}-gcc
export CPP=${HOST}-g++

export INSTALL_PREFIX=/usr
export INSTALL_BOOTDIR=/boot
export INSTALL_LIBDIR=$INSTALL_PREFIX/lib
export INSTALL_INCLUDEDIR=$INSTALL_PREFIX/include

export CFLAGS='-O0 -g'
export CPPFLAGS='-fno-exceptions -fno-rtti -Wno-unused-function'

# configure the cross-copmiler to use the desired system root
export PROJECTROOT="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
export SYSROOT="$PROJECTROOT/sysroot"
export CC="$CC --sysroot=$SYSROOT"
export CPP="$CPP --sysroot=$SYSROOT"

# work around that the -elf gcc targets doesn't have a system include dir because it was
# configured with --without-headers rather than --with-sysroot
if echo "$HOST" | grep -Eq -- '-elf($|-)'; then
    export CC="$CC -isystem=$INSTALL_INCLUDEDIR"
    export CPP="$CPP -isystem=$INSTALL_INCLUDEDIR"
fi
