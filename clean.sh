#!/bin/bash
set -e

cd libc
./clean.sh

cd ../kernel
./clean.sh

cd ..
rm -rf sysroot
rm -rf isodir
rm -rf myos.iso
