#!/bin/bash
set -e
source ./config.sh

cd libc
make install-headers

cd ../kernel
make install-headers

cd ../libc
make install

cd ../kernel
make install

cd ..

#cd libc
#./install.sh

#cd ../kernel
#./install.sh

#cd ..
