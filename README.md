Bare-bones UNIX clone OS.  
Following [OSDev wiki tutorials](https://wiki.osdev.org/Meaty_Skeleton) and [James Molloy's kernel development tutorials](http://www.jamesmolloy.co.uk/tutorial_html/).
