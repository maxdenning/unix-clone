#include "paging.h"
#include "kmalloc.h"
#include <string.h>

namespace kernel::paging
{


template<size_t B> class Bitset
{
    public:
        Bitset()
        {
            memset(m_data, 0U, m_data_size);
        }

        ~Bitset() = default;

        void set(size_t index, bool value = true)
        {
            //TODO: check parameters
            if (value)
            {
                m_data[index / 8U] |= 1U << (index % 8U);
            }
            else
            {
                m_data[index / 8U] &= ~(1U << (index % 8U));
            }
        }

        bool test(size_t index) const
        {
            //TODO: check parameters
            return (m_data[index / 8U] & (1U << (index % 8U))) != 0U;
        }

        constexpr bits() const
        {
            return B;
        }

        size_t size() const
        {
            return m_data_size;
        }

        uint8_t operator[](size_t index) const
        {
            return m_data[index];
        }

    private:
        constexpr size_t m_data_size = (B / 8U) + (B % 8U != 0U);
        uint8_t m_data[m_data_size];
};



//using frameptr_t = uintptr_t;

constexpr static uint32_t PAGE_SIZE = 0x1000U;  // pages are 4KB (as are frames)
constexpr static uint32_t MEM_END_PAGE = 0x1000000U;  // assume size of physical memory is 16MB
constexpr static uint32_t nframes = MEM_END_PAGE / PAGE_SIZE;  // number of frames
//static uint32_t* frames_bitset;  // bitset of frames marked as used or available
//static size_t frames_bitset_size;  // size (bytes) of frames bitset (== index_from_bit(nframes))

static Bitset<nframes> frame_flags;
static page_directory_t* current_directory;

//static uint32_t index_from_bit(uint32_t a);
//static uint32_t offset_from_bit(uint32_t a);
//static void set_frame_index(frameptr_t frame, bool in_use);
//static uint32_t get_frame_index(frameptr_t frame_address);
//static void clear_frame(uint32_t frame);
//static void test_frame(uint32_t frame);
static uint32_t first_clear_frame_flag_index();


//////////////////////////////////////////////////////////////////////////////////////////////////
// PUBLIC FUNCTION DEFINITIONS
//////////////////////////////////////////////////////////////////////////////////////////////////
void paging_initialise()
{
    // initialise frame bitset
    //frames_bitset_size = index_from_bit(nframes); 
    //frames_bitset = (uint32_t*)kmalloc(frames_bitset_size);  // allocate bitset, page aligned
    //memset(frames_bitset, 0U, frames_bitset_size);

    // allocate a page directory
    page_directory_t* const kernel_directory = (page_directory_t*)kmalloc(sizeof(page_directory_t));
    memset(kernel_directory, 0U, sizeof(page_directory_t));
    current_directory = kernel_directory;
    
    // map physical addresses to virtual addresses from 0x0 to end of memory
    // so we can access it transparently / as if paging were not enabled


}

void switch_page_directory(page_directory_t* new_directory)
{

}

// returns a pointer to a page entry for a given address
page_entry_t* get_page(uintptr_t address, bool make, page_directory_t* const dir)
{
    // find the page table containing this address
    const size_t page_index = address / PAGE_SIZE;
    const size_t table_index = page_index / page_table_t::size;

    // if table is already assigned then get page from that table
    if (dir->tables[table_index] != nullptr)
    {
        return &dir->tables[table_index]->pages[page_index % page_table_t::size];
    }
    else if (make)
    {
        uintptr_t physical_address = nullptr;
        dir->tables[table_index] = (page_table_t*)kmalloc(sizeof(page_table_t), 0, physical_address);
        memset(dir->tables[table_index], 0U, PAGE_SIZE);

        dir->tables_physical[table_index] = phys | 0x7;  // present, read/write, user-mode
        return &dir->tables[table_index]->pages[page_index % page_table_t::size];
    }

}

void page_fault(dt::registers_t registers)
{

}



//////////////////////////////////////////////////////////////////////////////////////////////////
// PRIVATE FUNCTION DEFINITIONS
//////////////////////////////////////////////////////////////////////////////////////////////////
/*uint32_t index_from_bit(uint32_t a)
{
    return a / (8U * sizeof(uint32_t));
}

uint32_t offset_from_bit(uint32_t a)
{
    return a % (8U * sizeof(uint32_t));
}

static void set_frame_index(frameptr_t frame, bool in_use)
{
    const uint32_t idx = index_from_bit(frame / 0x1000);
    const uint32_t off = offset_from_bit(frame / 0x1000);
    frames[idx] |= 0x1 << off;
}

void clear_frame(uint32_t frame);
void test_frame(uint32_t frame);
*/

size_t first_clear_frame_flag_index()
{
    for(size_t i = 0U; i < frame_flags.size(); i++)
    {
        // no clear flags, move on
        if (frame_flags[i] == UINT8_MAX)
        {
            continue;
        }

        // check individual bits
        for(uint8_t j = 0U; j < 8U; j++)
        {
            const size_t index = (i * 8U) + j;
            if (frame_flags.test(index))
            {
                return index;
            }
        }
    }

    // no free frame flags
    return SIZE_MAX;
}

void allocate_frame(page_entry_t* page, bool is_user, bool is_writable)
{
    if (page->frame != 0)
    {
        return;  // frame is already allocated
        // return failure?
    }

    const size_t index = get_first_free_frame_index();
    if (index == SIZE_MAX)
    {
        // panic!
        // no free frames left
        // return failure?
    }

    set_frame_index(index, true);
    page->present = 1U;
    page->rw = is_writable ? 1 : 0;
    page->user = is_user ? 1 : 0;
    page->frame = index * 0x1000;

    // return success?
}

void free_frame(page_entry_t* page)
{
    if (page->frame == 0)
    {
        return;  // given page did not have allocated frame
        // return failure?
    }

    frame_flags.set(page->frame / PAGE_SIZE, false);  // mark frame as free in array
    page->frame = 0;

    // return success?
}





}
