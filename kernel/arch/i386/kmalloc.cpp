#include "kmalloc.h"

namespace kernel
{

static uintptr_t kmalloc_address = 0U;

static bool is_power_of_2(int8_t i);
static int8_t round_up_to_nearest_power_of_2(int8_t i);

//////////////////////////////////////////////////////////////////////////////////////////////////
// PUBLIC FUNCTION DEFINITIONS
//////////////////////////////////////////////////////////////////////////////////////////////////
uintptr_t kmalloc(size_t nbytes, int8_t align)
{
    if (align <= 0)
    {
        // page align (if not aligned already)
        if (kmalloc_address & 0x00000FFF)
        {
            kmalloc_address &= 0x00000FFF;
            kmalloc_address += 0x1000;
        }
    }
    else
    {
        // align to specified size
        if (!is_power_of_2(align))
        {
            align = round_up_to_nearest_power_of_2(align);
        }

        // round up to nearest alignment (if not aligned already)
        if (kmalloc_address % align != 0)
        {
            kmalloc_address += align - (kmalloc_address % align);
        }
    }

    uintptr_t addr = kmalloc_address;
    kmalloc_address += nbytes;
    return addr;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
// PRIVATE FUNCTION DEFINITIONS
//////////////////////////////////////////////////////////////////////////////////////////////////
bool is_power_of_2(int8_t i)
{
    return i & (i - 1) == 0;
}

int8_t round_up_to_nearest_power_of_2(int8_t i)
{
    i--;
    i |= i >> 1;
    i |= i >> 2;
    i |= i >> 4;
    i++;
    return i;
}


}
