#ifndef _KERNEL_KMALLOC_H
#define _KERNEL_KMALLOC_H

#include <stdint.h>
#include <stddef.h>

namespace kernel
{


uintptr_t kmalloc(size_t nbytes, int8_t align = 0);


}
#endif
