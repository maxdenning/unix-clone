#ifndef _KERNEL_PORT_IO_H
#define _KERNEL_PORT_IO_H

#include <stdint.h>

namespace kernel::port_io
{


static void outb(uint16_t port, uint8_t value)
{
    asm volatile ("outb %1, %0" : : "dN" (port), "a" (value));
}

static uint8_t inb(uint16_t port)
{
    uint8_t ret;
    asm volatile("inb %1, %0" : "=a" (ret) : "dN" (port));
    return ret;
}

static uint16_t inw(uint16_t port)
{
    uint16_t ret;
    asm volatile("inw %1, %0" : "=a" (ret) : "dN" (port));
    return ret;
}

static inline void io_wait()
{
    // force cpu to wait for I/O operation to complete
    // should only be used when there is no status register or IRQ to report that info is received
    // port 0x80 is used for 'checkpoints' during POST, linux kenrel seems to think it is free for use
    asm volatile ("outb %%al, $0x80" : : "a"(0));
}


}
#endif
