#ifndef _KERNEL_PAGING_H
#define _KERNEL_PAGING_H

#include "isr.hpp"
#include <stdint.h>
#include <stddef.h>

namespace kernel::paging
{


struct page_entry_t
{
    uint32_t present    : 1;  // is page present in memory
    uint32_t rw         : 1;  // 0 = read only, 1 = read/write
    uint32_t user       : 1;  // 0 = kernel-mode page, 1 = user-mode page
    uint32_t accessed   : 1;  // has page been accessed since last refresh?
    uint32_t dirty      : 1;  // has page been written to since last refresh?
    uint32_t            : 7;  // amalgamation of unused and reserved bits
    uint32_t frame      : 20; // frame address (shifted right 12 bits)
};

struct page_table_t
{
    constexpr static size_t size = 1024U;
    page_entry_t pages[size];
};

struct page_directory_t
{
    constexpr static size_t size = 1024U;
    page_table_t* tables[size];  // pointers to virtual address of page tables
    uintptr_t tables_physical[size];  // pointers to physical address of page tables
//    uintptr_t physical_address;  // physical address of tables_physial (useful when kernel is heap allocated)
};

void paging_initialise(void);
void switch_page_directory(page_directory_t* new_directory);  // loads specified register into the CR3 directory
page_entry_t* get_page(uintptr_t address, int make, page_directory_t* dir);
void page_fault(dt::registers_t registers);


}
#endif
