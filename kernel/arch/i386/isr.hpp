#ifndef _KERNEL_ISR_H
#define _KERNEL_ISR_H

#include <stdint.h>

namespace kernel::dt
{


// NOTE: PIC = programmable interrupt controller
constexpr uint16_t PIC_M_COMMAND = 0x20;  // io base address for master PIC, command port
constexpr uint16_t PIC_S_COMMAND = 0xA0;  // io base address for slave PIC, command port
constexpr uint16_t PIC_M_DATA = 0x21;  // master PIC data port
constexpr uint16_t PIC_S_DATA = 0xA1;  // slave PIC data port
constexpr uint16_t PIC_CMD_EOI = 0x20;  // end of interrupt command code

struct registers_t
{
    uint32_t ds;  // data segment selector
    uint32_t edi, esi, ebp, _, ebx, edx, ecx, eax;  // pushed general purpose registers
    uint32_t interrupt_number;  // interrupt number
    uint32_t interrupt_error_code;  // interrupt error code
    uint32_t eip, cs, eflags, esp, ss;  // pushed processor state registers
};

using isr_handler_t = void(*)(const registers_t* const);

constexpr uint8_t irq_to_isr(const uint8_t irq)
{
    return irq + 32U;
}

constexpr uint8_t isr_to_irq(const uint8_t isr)
{
    return isr - 32U;
}

void isr_register_handler(uint8_t isr_number, isr_handler_t handler);


}


// allow access to the addresses of the corresponding ISR handlers defined in ASM.
extern "C" void isr0(void);  // divide by zero exception
extern "C" void isr1(void);  // debug exception
extern "C" void isr2(void);  // non-maskable interrupt
extern "C" void isr3(void);  // breakpoint exception
extern "C" void isr4(void);  // into detected overflow
extern "C" void isr5(void);  // out of bounds exception
extern "C" void isr6(void);  // invalid opcode exception
extern "C" void isr7(void);  // no coprocessor exception
extern "C" void isr8(void);  // double fault (pushes error code)
extern "C" void isr9(void);  // coprocessor segment overrun
extern "C" void isr10(void);  // bad TSS (pushes error code)
extern "C" void isr11(void);  // segment not present (pushes error code)
extern "C" void isr12(void);  // stack fault (pushes error code)
extern "C" void isr13(void);  // general protection fault (pushes error code)
extern "C" void isr14(void);  // page fault (pushes error code)
extern "C" void isr15(void);  // unknown interrupt exception
extern "C" void isr16(void);  // coprocessor fault
extern "C" void isr17(void);  // alignment check exception (pushes error code)
extern "C" void isr18(void);  // machine check exception
extern "C" void isr19(void);  // reserved
extern "C" void isr20(void);  // reserved
extern "C" void isr21(void);  // reserved (pushes error code)
extern "C" void isr22(void);  // reserved
extern "C" void isr23(void);  // reserved
extern "C" void isr24(void);  // reserved
extern "C" void isr25(void);  // reserved
extern "C" void isr26(void);  // reserved
extern "C" void isr27(void);  // reserved
extern "C" void isr28(void);  // reserved
extern "C" void isr29(void);  // reserved
extern "C" void isr30(void);  // reserved
extern "C" void isr31(void);  // reserved

// allow access to the addresses of the corresponding IRQ handlers defined in ASM.
extern "C" void irq0(void);  // master
extern "C" void irq1(void);  // master
extern "C" void irq2(void);  // master
extern "C" void irq3(void);  // master
extern "C" void irq4(void);  // master
extern "C" void irq5(void);  // master
extern "C" void irq6(void);  // master
extern "C" void irq7(void);  // master
extern "C" void irq8(void);  // slave
extern "C" void irq9(void);  // slave
extern "C" void irq10(void);  // slave
extern "C" void irq11(void);  // slave
extern "C" void irq12(void);  // slave
extern "C" void irq13(void);  // slave
extern "C" void irq14(void);  // slave
extern "C" void irq15(void);  // slave


#endif
