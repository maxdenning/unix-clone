#include "kernel/timer.hpp"
#include "isr.hpp"
#include "port_io.hpp"

namespace kernel::timer
{

//NOTE: PIT = programmable interval timer
constexpr static uint16_t PIT_CHANNEL_0 = 0x40;  // PIT channel 0, output connected to IRQ0
constexpr static uint16_t PIT_CHANNEL_1 = 0x41;  // PIT channel 1, not useful, not implemented
constexpr static uint16_t PIT_CHANNEL_2 = 0x42;  // PIT channel 2, controls PC speaker, not useful here
constexpr static uint16_t PIT_COMMAND = 0x43;  // PIT command port
constexpr static uint32_t PIT_FREQ = 1193180U;  // Hz, maximum interrupt frequency
constexpr static uint8_t PIT_REPEAT_MODE = 0x36;  // command enabling repeating mode


//////////////////////////////////////////////////////////////////////////////////////////////////
// PUBLIC FUNCTION DEFINITIONS
//////////////////////////////////////////////////////////////////////////////////////////////////
void timer_initialise(uint32_t freq)
{
    // set the PIT to repeating mode, so when the divisor counter reaches zero it is automatically
    // refreshed
    kernel::port_io::outb(PIT_COMMAND, PIT_REPEAT_MODE);

    // divide PIT input clock by divisor to get the requested frequency 
    const uint16_t divisor = PIT_FREQ / freq;

    // divisor is sent byte-wise, so split into upper and lower then send
    kernel::port_io::outb(PIT_CHANNEL_0, divisor & 0xFF);
    kernel::port_io::outb(PIT_CHANNEL_0, (divisor >> 8) & 0xFF);
}


}
