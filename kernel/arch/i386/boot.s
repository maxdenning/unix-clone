/* Declare constants for the multiboot header. */
.set ALIGN,     1<<0                /* align loaded modules on page boundaries */
.set MEMINFO,   1<<1                /* provide memory map */
.set FLAGS,     ALIGN | MEMINFO     /* this is the multiboot flag field */
.set MAGIC,     0x1BADB002          /* magic number lets bootloader find the header */
.set CHECKSUM,  -(MAGIC + FLAGS)    /* checksum of above, to prove we are multiboot */

/*
Declare a multiboot header that marks this as the kernel. There are magic numbers documented in
the multiboot standard. The bootloader will search for this signature in the first 8KiB of the
kernal file, aligned at a 32-bit boundary. The signature is in its own section so the header is
forced to be within with limit. 
*/
.section .multiboot                 /* creates and enters new section */
.align 4
.long MAGIC                         /* long type is 32 bit */
.long FLAGS
.long CHECKSUM

/*
The multiboot standard does not define the value of the stack pointer register (ESP) and the
kernal must provide a stack. This allocates room for a small stack by creating a symbol at the
bottom, allocating 16384 bytes (16KB), and then creating a symbol at the top. The stack grows down
in x86. The stack is in its own section so it can be marked nobits, which means the kernel file is
smaller because it does not contain an uninitialised stack. The stack on x86 must be 16 byte
aligned (System V ABI standard). The compiler assumes the stack is properly aligned.
*/
.section .bss                       /* block starting symbol */
.align 16
stack_bottom:                       /* global scope symbolic label, can be used as an operand */
.skip 16384                         /* 16 KB */
stack_top:

/*
The linker script specifies _start as the entry point to the kernel. The bootloader will jump to
this position once the kernel has been loaded. It doesn't make sense to return from this function
as the bootloader is gone.
*/
.section .text                      /* separates data (.data) and executable (.text) section */
.global _start
.type _start, @function
_start:
    /*
    The bootloader loads into 32-bit protected mode. Interrupts and paging are disabled. This
    state is defined by the multiboot standard. The kernel has full control of the CPU and can
    only make use of hardware features and any code it provides itself. There are no security
    restrictions, no safeguards, no debugging mechanisms.
    */

    /*
    Set up the stack by setting ESP to point to the top of the stack, since it grows downwards in
    x86.
    */
    movl $stack_top, %esp

    /*
    Initialise crucial processor state before entering the high level kernel. It is best to
    minimise the early environment where crucial features are offline. The CPU is not fully
    initialised yet: floating point instructions and instruction set extensions are not available.
    The GDT (global descriptor table) should be loaded here. Paging should be enabled here. C++
    features like global constructors and exceptions wlil require runtime support to work.
    */
    /*
    call _init  # call global constructors
    */

    /*
    Enter the high level kernel. The ABI requires the stack is 16-byte aligned at the time of the
    call instruction, which afterwards pushes the return pointer (4 bytes). The stack was 16 byte
    aligned above and a multiple of 16 bytes has been pushed to the stack since (i.e. 0). So the
    alignment has been preserved and the call is well defined.
    */
    call kernel_main

    /*
    If the system has nothing more to do put the computer into an infinite loop;
    1) Disable interrupts with cli (clear interrup enable in eflags). This is already disabled by
       by the bootloader.
    2) Wait for the next interrupt to arrive with hlt (halt instruction). Since they are disabled
       this will lock up the computer.
    3) Jump to the hlt instruction if it ever wakes up due to a non-maskable interrupt occurring
       or due to system management mode.  
    */
    cli
1:  hlt
    jmp 1b

/*
Set the size of the _start symbol to the current location (".") minus its start. This is useful
when debugging or when you implement call tracing.
*/
.size _state, . - _start
