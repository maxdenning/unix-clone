#include "kernel/descriptor_tables.hpp"
#include "isr.hpp"
#include "port_io.hpp"
#include <string.h>

namespace kernel::dt
{


/*
Access byte format:
    | 7 | 6 5 | 4  | 3 2 1 0 |
      P   PDL   DT   Type

    P: is segment present? 1 = true
    DPL: descriptor privilege level, ring 0 - 3  
    DT: descriptor type
    Type: segment type, i.e. code or data

Granularity byte format:
    | 7 | 6 | 5 | 4 | 3 2 1 0 |
      G   D   0   A   Segment length 19:16

    G: granularity, 0 = 1 byte, 1 = 1kb
    D: operand size, 0 = 16-bit, 1 = 32-bit
    0: zero
    A: available for system use, 0

    Bits 3..0 are the upper 4-bits of the limit, i.e. limit_high.
*/
struct __attribute__((packed)) gdt_entry_t
{
    uint16_t limit_low;  // lower 16-bits of the limit
    uint16_t base_low;  // lower 16-bits of the base
    uint8_t base_middle;  // next 8-bits of the base;
    uint8_t access;  // access flags, determines what ring this segment can be used in
    uint8_t granularity;
    uint8_t base_high;  // final 8-bits of the base
};

/*
Used to tell process where to find the GDT.
Base is the address of the first entry in the GDT.
Limit is the size of the table minus one (e.g. the last valid address in the table).
*/
struct __attribute__((packed)) gdt_ptr_t
{
    uint16_t limit;  // upper 16-bits of all selector limits
    uint32_t base;  // address of the first gdt entry
};

/*
Describes an interrupt gate.

Flags byte format:
    | 7 | 6  5 | 4 | 3 2 1 0 |
      P   DPL    S   Type
    
    P: signifies entry is prent, if clear causes an "interrupt not handled" exception
    DPL: expected minimum caller privilege level (ring)
    S: storage segment (0 for interrupt and trap gates)
    Type: gate type, here we use 32bit interrupt gate (0b1110)

Gate types:
    0b0101: 32 bit task gate
    0b0110: 16-bit interrupt gate
    0b0111: 16-bit trap gate
    0b1110: 32-bit interrupt gate
    0b1111: 32-bit trap gate

    Interrupt and trap gates are similar, although for interrupt gates inerrupts are disabled on
    entry and re-enabled on exit (IRET) which restores the saves eflags.
*/
struct __attribute__((packed)) idt_entry_t
{
    uint16_t base_low;  // lower 16-bits of the address to jump to when this interrupt fires
    uint16_t selector;  // kernel segment selector
    uint8_t zero;  // must always be 0
    uint8_t flags;  // gate type and attributes
    uint16_t base_high;  // upper 16-bits of the address to jump to when this interrupt fires
};

/*
Points to an array of interrupt handlers in a suitable format to give to "lidt".
*/
struct __attribute__((packed)) idt_ptr_t
{
    uint16_t limit;
    uint32_t base;  // address fo the first element in the idt entry array
};


constexpr static uint16_t GDT_MAX_ENTRIES = 5U;
constexpr static uint16_t IDT_MAX_ENTRIES = 256U;

static gdt_entry_t gdt_entries[GDT_MAX_ENTRIES];
static gdt_ptr_t gdt_ptr;
static idt_entry_t idt_entries[IDT_MAX_ENTRIES];
static idt_ptr_t idt_ptr;

static void gdt_initialise(void);
static void gdt_set_gate(int32_t, uint32_t, uint32_t, uint8_t, uint8_t);
extern "C" void gdt_flush(uint32_t gdt_ptr);  // defined in asm, gdt_flus.s
static void idt_initialise(void);
static void idt_set_gate(uint8_t, uint32_t, uint16_t, uint8_t);
static void idt_remap_irq(void);
extern "C" void idt_flush(uint32_t idt_ptr);  // defined in asm, idt_flush.s


//////////////////////////////////////////////////////////////////////////////////////////////////
// PUBLIC FUNCTION DEFINITIONS
//////////////////////////////////////////////////////////////////////////////////////////////////
void dt_initialise(void)
{
    gdt_initialise();
    idt_initialise();
}

void register_interrupt_handler(uint8_t interrupt, void(*handler)(const void* const))
{
    isr_register_handler(interrupt, (isr_handler_t)handler);
}


//////////////////////////////////////////////////////////////////////////////////////////////////
// PRIVATE FUNCTION DEFINITIONS
//////////////////////////////////////////////////////////////////////////////////////////////////
void gdt_initialise(void)
{
    // GDT has 5 entries: kernel code segment, kernel data segment, user code segment, user data
    // segment, and a null entry
    gdt_ptr.limit = (sizeof(gdt_entry_t) * GDT_MAX_ENTRIES) - 1U;
    gdt_ptr.base = (uint32_t)&gdt_entries;  // point to start of table

    // Set up the five entries. The only thing that changes is the access byte's type and DPL
    // fields, i.e. segment type and privilege.
    gdt_set_gate(0U, 0U, 0U,          0U,    0U);
    gdt_set_gate(1U, 0U, 0xFFFFFFFFU, 0x9AU, 0xCFU);
    gdt_set_gate(2U, 0U, 0xFFFFFFFFU, 0x92U, 0xCFU);
    gdt_set_gate(3U, 0U, 0xFFFFFFFFU, 0xFAU, 0xCFU);
    gdt_set_gate(4U, 0U, 0xFFFFFFFFU, 0xF2U, 0xCFU);

    gdt_flush((uint32_t)&gdt_ptr);
}

void gdt_set_gate(int32_t entry_index, uint32_t base, uint32_t limit, uint8_t access, uint8_t granularity)
{
    gdt_entries[entry_index].base_low    = base & 0xFFFF;
    gdt_entries[entry_index].base_middle = (base >> 16) & 0xFF;
    gdt_entries[entry_index].base_high   = (base >> 24) & 0xFF;

    gdt_entries[entry_index].limit_low   = limit & 0xFFFF;
    gdt_entries[entry_index].granularity = (limit >> 16) & 0xFF;

    gdt_entries[entry_index].granularity |= granularity & 0xF0;
    gdt_entries[entry_index].access      = access;
}

void idt_initialise(void)
{
    // setup interrupt descriptor table
    idt_ptr.limit = (sizeof(idt_entry_t) * IDT_MAX_ENTRIES) - 1U;
    idt_ptr.base = (uint32_t)&idt_entries;

    memset(&idt_entries, 0, sizeof(idt_entry_t) * IDT_MAX_ENTRIES);

    // remap interrupt request table so incoming request numbers do not overlap with reserved
    // processor interrupt numbers (0..31).
    idt_remap_irq();

    // populate first 32 interrupt entries, used by the processor to signal the kernal
    idt_set_gate( 0U, (uint32_t)isr0,  0x08, 0x8E);
    idt_set_gate( 1U, (uint32_t)isr1,  0x08, 0x8E);
    idt_set_gate( 2U, (uint32_t)isr2,  0x08, 0x8E);
    idt_set_gate( 3U, (uint32_t)isr3,  0x08, 0x8E);
    idt_set_gate( 4U, (uint32_t)isr4,  0x08, 0x8E);
    idt_set_gate( 5U, (uint32_t)isr5,  0x08, 0x8E);
    idt_set_gate( 6U, (uint32_t)isr6,  0x08, 0x8E);
    idt_set_gate( 7U, (uint32_t)isr7,  0x08, 0x8E);
    idt_set_gate( 8U, (uint32_t)isr8,  0x08, 0x8E);
    idt_set_gate( 9U, (uint32_t)isr9,  0x08, 0x8E);
    idt_set_gate(10U, (uint32_t)isr10, 0x08, 0x8E);
    idt_set_gate(11U, (uint32_t)isr11, 0x08, 0x8E);
    idt_set_gate(12U, (uint32_t)isr12, 0x08, 0x8E);
    idt_set_gate(13U, (uint32_t)isr13, 0x08, 0x8E);
    idt_set_gate(14U, (uint32_t)isr14, 0x08, 0x8E);
    idt_set_gate(15U, (uint32_t)isr15, 0x08, 0x8E);
    idt_set_gate(16U, (uint32_t)isr16, 0x08, 0x8E);
    idt_set_gate(17U, (uint32_t)isr17, 0x08, 0x8E);
    idt_set_gate(18U, (uint32_t)isr18, 0x08, 0x8E);
    idt_set_gate(19U, (uint32_t)isr19, 0x08, 0x8E);
    idt_set_gate(20U, (uint32_t)isr20, 0x08, 0x8E);
    idt_set_gate(21U, (uint32_t)isr21, 0x08, 0x8E);
    idt_set_gate(22U, (uint32_t)isr22, 0x08, 0x8E);
    idt_set_gate(23U, (uint32_t)isr23, 0x08, 0x8E);
    idt_set_gate(24U, (uint32_t)isr24, 0x08, 0x8E);
    idt_set_gate(25U, (uint32_t)isr25, 0x08, 0x8E);
    idt_set_gate(26U, (uint32_t)isr26, 0x08, 0x8E);
    idt_set_gate(27U, (uint32_t)isr27, 0x08, 0x8E);
    idt_set_gate(28U, (uint32_t)isr28, 0x08, 0x8E);
    idt_set_gate(29U, (uint32_t)isr29, 0x08, 0x8E);
    idt_set_gate(30U, (uint32_t)isr30, 0x08, 0x8E);
    idt_set_gate(31U, (uint32_t)isr31, 0x08, 0x8E);

    // populate idt entried 32..47, now mapped to interrupt requests
    idt_set_gate(irq_to_isr( 0U), (uint32_t)irq0,  0x08, 0x8E);
    idt_set_gate(irq_to_isr( 1U), (uint32_t)irq1,  0x08, 0x8E);
    idt_set_gate(irq_to_isr( 2U), (uint32_t)irq2,  0x08, 0x8E);
    idt_set_gate(irq_to_isr( 3U), (uint32_t)irq3,  0x08, 0x8E);
    idt_set_gate(irq_to_isr( 4U), (uint32_t)irq4,  0x08, 0x8E);
    idt_set_gate(irq_to_isr( 5U), (uint32_t)irq5,  0x08, 0x8E);
    idt_set_gate(irq_to_isr( 6U), (uint32_t)irq6,  0x08, 0x8E);
    idt_set_gate(irq_to_isr( 7U), (uint32_t)irq7,  0x08, 0x8E);
    idt_set_gate(irq_to_isr( 8U), (uint32_t)irq8,  0x08, 0x8E);
    idt_set_gate(irq_to_isr( 9U), (uint32_t)irq9,  0x08, 0x8E);
    idt_set_gate(irq_to_isr(10U), (uint32_t)irq10, 0x08, 0x8E);
    idt_set_gate(irq_to_isr(11U), (uint32_t)irq11, 0x08, 0x8E);
    idt_set_gate(irq_to_isr(12U), (uint32_t)irq12, 0x08, 0x8E);
    idt_set_gate(irq_to_isr(13U), (uint32_t)irq13, 0x08, 0x8E);
    idt_set_gate(irq_to_isr(14U), (uint32_t)irq14, 0x08, 0x8E);
    idt_set_gate(irq_to_isr(15U), (uint32_t)irq15, 0x08, 0x8E);

    // tell processor where to find this interrupt descriptor table
    idt_flush((uint32_t)&idt_ptr);
}

void idt_set_gate(uint8_t entry_index, uint32_t base, uint16_t selector, uint8_t flags)
{
    idt_entries[entry_index].base_low = base & 0xFFFF;
    idt_entries[entry_index].base_high = (base >> 16) & 0xFFFF;

    idt_entries[entry_index].selector = selector;
    idt_entries[entry_index].zero = 0U;

    //TODO: when going to user-mode must uncomment line below to set gate privilege to 3
    idt_entries[entry_index].flags = flags /*| 0x60*/;
}

void idt_remap_irq(void)
{
    constexpr uint8_t ICW1_INIT = 0x11;  // initialisation command
    constexpr uint8_t ICW4_8086 = 0x01;  // 8086/88 (MCS-80/85) mode

    const uint8_t pic_m_data_mask = port_io::inb(PIC_M_DATA);  // save masks
    const uint8_t pic_s_data_mask = port_io::inb(PIC_S_DATA);
    
    port_io::outb(PIC_M_COMMAND, ICW1_INIT);  // send initialise command
    port_io::io_wait();
    port_io::outb(PIC_S_COMMAND, ICW1_INIT);
    port_io::io_wait();

    port_io::outb(PIC_M_DATA, 0x20);  // ICW2, vector offset
    port_io::io_wait();
    port_io::outb(PIC_S_DATA, 0x28);
    port_io::io_wait();

    port_io::outb(PIC_M_DATA, 0x04);  // ICW3, tell master there is a slave at IRQ2
    port_io::io_wait();
    port_io::outb(PIC_S_DATA, 0x02);  // ICW3, tell slave its cascade identity
    port_io::io_wait();

    port_io::outb(PIC_M_DATA, ICW4_8086);  // ICW4, gives additional info about env
    port_io::io_wait();
    port_io::outb(PIC_S_DATA, ICW4_8086);
    port_io::io_wait();

    port_io::outb(PIC_M_DATA, pic_m_data_mask);  // restore saved masks
    port_io::outb(PIC_S_DATA, pic_s_data_mask);
}



}
