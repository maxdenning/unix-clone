#include "kernel/tty.hpp"
#include "kernel/tty.h"
#include "port_io.hpp"
#include <stddef.h>
#include <stdint.h>

namespace kernel::tty
{


static constexpr size_t VGA_WIDTH = 80U;
static constexpr size_t VGA_HEIGHT = 25U;
static constexpr uint64_t const VGA_MEMORY = 0xB8000;

static size_t tty_row = 0U;
static size_t tty_column = 0U;
static uint16_t* tty_buffer = nullptr;
static vga::VGAEntryColour tty_colour_default = 0U;

static void tty_move_cursor(size_t row, size_t column);
static void tty_put_char(char c, vga::VGAEntryColour col);
static void tty_put_entry_at(uint16_t entry, size_t x, size_t y);
static void tty_scroll();
static uint8_t count_hex_digits(uint32_t value);
static uint8_t count_dec_digits(uint32_t value);
template<typename T> static T pow(T base, T exponent);


//////////////////////////////////////////////////////////////////////////////////////////////////
// PUBLIC FUNCTION DEFINITIONS
//////////////////////////////////////////////////////////////////////////////////////////////////
void tty_initialise(void)
{
    tty_row = 0U;
    tty_column = 0U;
    tty_buffer = (uint16_t*)(VGA_MEMORY);
    tty_colour_default = vga::vga_entry_colour(vga::COLOR_LIGHT_GREY, vga::COLOR_BLACK);
    tty_clear();
}

void tty_clear(void)
{
    const uint16_t blank = vga::vga_entry(' ', vga::vga_entry_colour(vga::COLOR_WHITE, vga::COLOR_BLACK));
    for(size_t i = 0U; i < VGA_HEIGHT * VGA_WIDTH; i++)
    {
        tty_buffer[i] = blank;
    }

    // move cursor back to start
    tty_row = 0U;
    tty_column = 0U;
    tty_move_cursor(tty_row, tty_column);
}

void tty_set_colour(vga::VGAEntryColour col)
{
    tty_colour_default = col;
}

void tty_write(char c, vga::VGAEntryColour col)
{
    tty_put_char(c, col);
}

void tty_write(const char* const str, size_t len, vga::VGAEntryColour col)
{
    for(size_t i = 0U; i < len; i++)
    {
        tty_put_char(str[i], col);
    }
}

void tty_write(const char* const str, vga::VGAEntryColour col)
{
    size_t i = 0U;
    while(str[i])
    {
        tty_put_char(str[i], col);
        i++;
    }
}

void tty_write_hex(uint32_t hex, vga::VGAEntryColour col)
{
    int16_t len = count_hex_digits(hex);
    tty_put_char('0', col);
    tty_put_char('x', col);

    constexpr char lut[] = "0123456789ABCDEF";
    while(len--)
    {
        tty_put_char(lut[(hex >> (4U * len)) & 0xf], col);
    }
}

void tty_write_dec(uint32_t dec, vga::VGAEntryColour col)
{
    uint32_t len = count_dec_digits(dec);
    while(len--)
    {
        char c = '0' + (dec / pow<uint32_t>(10U, len)) % 10;
        tty_put_char(c, col);
    }
}


//////////////////////////////////////////////////////////////////////////////////////////////////
// PRIVATE FUNCTION DEFINITIONS
//////////////////////////////////////////////////////////////////////////////////////////////////
void tty_move_cursor(size_t row, size_t column)
{
    // updates hardware cursor
    // cursor location is 16 bits wide, the VGA controller accepts this as two bytes
    const uint16_t cursor_location = (row * VGA_WIDTH) + column;
    port_io::outb(0x3D4, 14);  // tell VGA controller that we are setting the high byte
    port_io::outb(0x3D5, cursor_location >> 8);  // send high cursor byte
    port_io::outb(0x3D4, 15);  // tell VGA controller that we are setting the low byte
    port_io::outb(0x3D5, cursor_location);  // send low cursor byte
}

void tty_put_char(char c, vga::VGAEntryColour col)
{
    // handle special characters
    switch (c)
    {
    case 0x08:  // handle backspace
        if (tty_column > 0U)
        {
            tty_column--;
        }
        break;
    
    case 0x09:  // handle tab, move to multiple of 4
        tty_column = (tty_column + 4U) & ~(4U - 1U);
        break;

    case '\r':  // handle carriage return
        tty_column = 0U;
        break;

    case '\n':
        tty_column = 0U;
        tty_row++;
        break;

    default:
        break;
    }

    // handle printable characters
    if (c >= ' ')
    {
        if (col == static_cast<vga::VGAColour>(0U))
        {
            col = vga::vga_entry_colour(vga::COLOR_LIGHT_GREY, vga::COLOR_BLACK);
        }
        tty_put_entry_at(vga::vga_entry(c, col), tty_column, tty_row);
        tty_column++;
    }

    // check if new row needed
    if (tty_column >= VGA_WIDTH)
    {
        tty_column = 0U;
        tty_row++;
    }

    // check if scroll needed
    if (tty_row >= VGA_HEIGHT)
    {
        tty_scroll();
    }

    // update cursor
    tty_move_cursor(tty_row, tty_column);
}

void tty_put_entry_at(uint16_t vga_entry, size_t x, size_t y)
{
    tty_buffer[(y * VGA_WIDTH) + x] = vga_entry; //vga_entry(c, col);
}

void tty_scroll()
{
    for(size_t i = 0U; i < (VGA_HEIGHT - 1U) * VGA_WIDTH; i++)
    {
        tty_buffer[i] = tty_buffer[i + VGA_WIDTH];
    }

    const uint16_t blank = vga::vga_entry(' ', vga::vga_entry_colour(vga::COLOR_WHITE, vga::COLOR_BLACK));
    for(size_t i = (VGA_HEIGHT - 1U) * VGA_WIDTH; i < VGA_HEIGHT * VGA_WIDTH; i++)
    {
        tty_buffer[i] = blank;
    }

    tty_row = VGA_HEIGHT - 1U;
}

uint8_t count_hex_digits(uint32_t value)
{
    uint8_t count = 1U;
    while(value >>= 4U)
    {
        count++;
    }
    return count;
}

uint8_t count_dec_digits(uint32_t value)
{
    uint8_t count = 1U;
    while(value /= 10U)
    {
        count++;
    }
    return count;
}

template<typename T> T pow(T base, T exponent)
{
    T result = 1;
    while(exponent--)
    {
        result *= base;
    }
    return result;
}


}


//
// tty.h
//
void tty_write_c(char c)
{
    kernel::tty::tty_write(c);
}

void tty_write_s(const char* const str)
{
    kernel::tty::tty_write(str);
}
/*
void tty_write_sn(const char* const str, size_t len)
{
    kernel::tty::tty_write(str, len);
}

void tty_write_hex(uint32_t hex)
{
    kernel::tty::tty_write_hex(hex);
}

void tty_write_dec(uint32_t dec)
{
    kernel::tty::tty_write_dec(dec);
}
*/
