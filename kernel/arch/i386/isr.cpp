#include "isr.hpp"
#include "kernel/tty.hpp"
#include "port_io.hpp"
#include <stdio.h>

namespace kernel::dt
{


static isr_handler_t isr_interrupt_handlers[256U] = { nullptr };  // TODO: memset this to 0 properly

static bool is_irq_slave(uint8_t isr_number);
static void irq_send_eoi(uint8_t isr_number);  // send an EOI signal to the master and slave PICs


//////////////////////////////////////////////////////////////////////////////////////////////////
// PUBLIC FUNCTION DEFINITIONS
//////////////////////////////////////////////////////////////////////////////////////////////////
void isr_register_handler(uint8_t isr_number, isr_handler_t handler)
{
    isr_interrupt_handlers[isr_number] = handler;
}


//////////////////////////////////////////////////////////////////////////////////////////////////
// PRIVATE FUNCTION DEFINITIONS
//////////////////////////////////////////////////////////////////////////////////////////////////
bool is_irq_slave(uint8_t isr_number)
{
    return isr_number >= 40U;
}

void irq_send_eoi(uint8_t isr_number)
{
    if (kernel::dt::is_irq_slave(isr_number))
    {
        kernel::port_io::outb(kernel::dt::PIC_S_COMMAND, PIC_CMD_EOI);  // send to slave
    }
    kernel::port_io::outb(kernel::dt::PIC_M_COMMAND, PIC_CMD_EOI);  // send to master
}


}


extern "C" void isr_handler(const kernel::dt::registers_t* const registers)
{
    // call user provided interrupt handler if present
    if (kernel::dt::isr_interrupt_handlers[registers->interrupt_number] != nullptr)
    {
        kernel::dt::isr_interrupt_handlers[registers->interrupt_number](registers);
    }
    else
    {
        printf("\nISR! number=%#x, error=%#x\n", registers->interrupt_number, registers->interrupt_error_code);
    }
}

extern "C" void irq_handler(const kernel::dt::registers_t* const registers)
{
    // send reset signal
    kernel::dt::irq_send_eoi(registers->interrupt_number);

    printf("IRQ! number=%#x, error=%#x", registers->interrupt_number, registers->interrupt_error_code);

    // call user provided interrupt handler if present
    if (kernel::dt::isr_interrupt_handlers[registers->interrupt_number] != nullptr)
    {
        kernel::dt::isr_interrupt_handlers[registers->interrupt_number](registers);
    }
}
