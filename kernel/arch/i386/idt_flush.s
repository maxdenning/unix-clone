/*
*/

.global idt_flush                   # allow C code to call idt_flush

idt_flush:
    movl 4(%esp), %eax             # get pointer to the IDT, passed as a parameter
    lidt (%eax)                     # load the IDT pointer
    ret
