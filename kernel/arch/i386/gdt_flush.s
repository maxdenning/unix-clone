/*
Loads the first parameter (ESP + 4) into the GDT then loads the segment selectors for the data and
code segments. Each GDT entry is 8 bytes, and the kernel code descriptor is the second segment so
its offset is 0x08. The kernel data descriptor is the third, so its offset is 0x10.
*/

.global gdt_flush                   # allow C code to call gdt_flush
gdt_flush:
    movl 4(%esp), %eax              # get a pointer to the gdt, passed as a parameter
    lgdt (%eax)                     # load the new GDT pointer

    movw $0x10, %ax                 # 0x10 is the offset in the GDT to our data segment
    movw %ax, %ds                   # load all data segment selectors
    movw %ax, %es
    movw %ax, %fs
    movw %ax, %gs
    movw %ax, %ss

    jmp $0x08,$flush                # 0x08 is the offset to our code segment
flush:
    ret
