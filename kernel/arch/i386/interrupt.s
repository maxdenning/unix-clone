/*
Implements handlers for each specific interrupt number. Each one pushes the interrupt number and
an error code (or a dummy if there is no error code), before jumping to a generic handler.
*/

.macro ISR_NO_ERROR_CODE isr_number
    .global isr\isr_number
    isr\isr_number:
        # cli                       # disable interrupts  -- interrupts are automatically disabled
        push $0                     # push dummey error code
        push $\isr_number           # push interrupt number
        jmp isr_common_stub         # go to isr common handler
.endm

.macro ISR_ERROR_CODE isr_number
    .global isr\isr_number
    isr\isr_number:
        # cli                       # disable interrupts  -- interrupts are automatically disabled
        push $\isr_number           # push interrupt number
        jmp isr_common_stub         # go to isr common handler
.endm

ISR_NO_ERROR_CODE 0
ISR_NO_ERROR_CODE 1
ISR_NO_ERROR_CODE 2
ISR_NO_ERROR_CODE 3
ISR_NO_ERROR_CODE 4
ISR_NO_ERROR_CODE 5
ISR_NO_ERROR_CODE 6
ISR_NO_ERROR_CODE 7
ISR_ERROR_CODE 8
ISR_NO_ERROR_CODE 9
ISR_ERROR_CODE 10
ISR_ERROR_CODE 11
ISR_ERROR_CODE 12
ISR_ERROR_CODE 13
ISR_ERROR_CODE 14
ISR_NO_ERROR_CODE 15
ISR_NO_ERROR_CODE 16
ISR_ERROR_CODE 17
ISR_NO_ERROR_CODE 18
ISR_NO_ERROR_CODE 19
ISR_NO_ERROR_CODE 20
ISR_ERROR_CODE 21
ISR_NO_ERROR_CODE 22
ISR_NO_ERROR_CODE 23
ISR_NO_ERROR_CODE 24
ISR_NO_ERROR_CODE 25
ISR_NO_ERROR_CODE 26
ISR_NO_ERROR_CODE 27
ISR_NO_ERROR_CODE 28
ISR_NO_ERROR_CODE 29
ISR_NO_ERROR_CODE 30
ISR_NO_ERROR_CODE 31


# C callback for handling ISR interrupts 
.extern isr_handler

/*
Common ISR handler. Saves the processor state, sets up for kernel mode segments, calls the C-level
interrupt handler, and then restores the stack frame.
The processor state is automatically pushed to the stack before the ISR is passed here. IRET is
used to pop this state and return the processor to how it was before.
*/
isr_common_stub:
    pusha                           # push all general purpose registers to stack
    movw %ds, %ax                   # lower 16-bits of eax = ds
    push %eax                       # save the data segment descriptor

    movw $0x10, %ax                 # load the kernel segment descriptor
    movw %ax, %ds
    movw %ax, %es
    movw %ax, %fs
    movw %ax, %gs

    /*
    Need to pass register struct as a pointer, otherwise (i.e. in pass by value) the struct values
    will be in the wrong stack frame and could be invalidated by call to isr_handler.
    */
    lea (%esp), %eax                # get address of struct, which is equivalent to top of stack

    push %ebp                       # begin new stack frame for isr_handler
    movl %esp, %ebp                 #

    push %eax                       # push struct pointer to new stack frame as function argument

    call isr_handler                # call handler

    leave                           # leave isr_handler stack frame

    popl %eax                       # reload the original data segment descriptor
    movw %ax, %ds
    movw %ax, %es
    movw %ax, %fs
    movw %ax, %gs

    popa                            # pop all general purpose registers from stack
    addl $8, %esp                   # clean up the pushed error code and ISR number
    # sti                           # re-enable interrupts  -- interrupts are automatically re-enabled (through IRET of EFLAGS)
    iret                            # pops CS, EIP, EFLAGS, SS, and ESP


/*
Implements handlers for IRQs mapped to a certain ISR gate. Each one pushes the ISR number and an
error code (or a dummy if there is no error code), before jumping to a generic handler.
*/
.macro IRQ irq_number isr_number
    .global irq\irq_number
    irq\irq_number:
        push $0                     # push dummey error code
        push $\isr_number           # push interrupt number
        jmp irq_common_stub         # go to irq common handler
.endm

IRQ  0, 32
IRQ  1, 33
IRQ  2, 34
IRQ  3, 35
IRQ  4, 36
IRQ  5, 37
IRQ  6, 38
IRQ  7, 39
IRQ  8, 40
IRQ  9, 41
IRQ 10, 42
IRQ 11, 43
IRQ 12, 44
IRQ 13, 45
IRQ 14, 46
IRQ 15, 47

# C callback for handling IRQ interrupts 
.extern irq_handler

/*
Common IRQ handler. Saves the processor state, sets up for kernel mode segments, calls the C-level
interrupt handler, and then restores the stack frame.
The processor state is automatically pushed to the stack before the IRQ is passed here. IRET is
used to pop this state and return the processor to how it was before.
*/
irq_common_stub:
    pusha
    movw %ds, %ax                   # lower 16-bits of eax = ds
    push %eax                       # save the data segment descriptor

    movw $0x10, %ax                 # load the kernel segment descriptor
    movw %ax, %ds
    movw %ax, %es
    movw %ax, %fs
    movw %ax, %gs

    /*
    Need to pass register struct as a pointer, otherwise (i.e. in pass by value) the struct values
    will be in the wrong stack frame and could be invalidated by call to isr_handler.
    */
    lea (%esp), %eax                # get address of struct, which is equivalent to top of stack

    push %ebp                       # begin new stack frame for isr_handler
    movl %esp, %ebp                 #

    push %eax                       # push struct pointer to new stack frame as function argument

    call irq_handler                # call handler

    leave                           # leave isr_handler stack frame

    popl %ebx                       # reload the original data segment descriptor
    movw %bx, %ds
    movw %bx, %es
    movw %bx, %fs
    movw %bx, %gs

    popa                            # pop all general purpose registers from stack
    addl $8, %esp                   # clean up the pushed error code and ISR number
    iret                            # pops CS, EIP, EFLAGS, SS, and ESP
