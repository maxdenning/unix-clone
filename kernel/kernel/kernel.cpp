#include "kernel/vga.hpp"
#include "kernel/tty.hpp"
#include "kernel/descriptor_tables.hpp"
#include "kernel/timer.hpp"
#include <stdio.h>

// check if compiler thinks wrong OS is being targetted
#ifdef __linux__
#error "You are not using the cross-compiler."
#endif

// allow only 32-bit ix86 targets
#ifndef __i386__
#error "Requires ix86-elf compiler."
#endif

using namespace kernel;


void timer_callback(const void* const registers)
{
    static uint32_t tick = 0;
    tick++;
    tty::tty_write("tick ");
    tty::tty_write_dec(tick);
    tty::tty_write("\n");
}

void isr03_handler(const void* const registers)
{
    printf("~interrupt 03\n");
}

void isr04_handler(const void* const registers)
{
    printf("~interrupt 04\n");
}


extern "C" void kernel_main(void)
{
    tty::tty_initialise();
    tty::tty_set_colour(vga::vga_entry_colour(vga::COLOR_LIGHT_GREY, vga::COLOR_BLACK));

    dt::dt_initialise();
    timer::timer_initialise(50U);  // tick at 50Hz

    tty::tty_write("hello kernel\n");
    
    dt::register_interrupt_handler(32U, timer_callback);

    dt::register_interrupt_handler(3U, isr03_handler);
    dt::register_interrupt_handler(4U, isr04_handler);

    asm volatile ("int $0x3");
    asm volatile ("int $0x4"); 





    
    

    uint32_t x = 0U;
    for(;;)
    {
        x++;
    }
    printf("%i", x);


    tty::tty_write("end");
}

