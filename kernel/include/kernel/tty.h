#ifndef _KERNEL_TTY_H
#define _KERNEL_TTY_H

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif


// compatability with libc, e.g. printf
void tty_write_c(char c);
void tty_write_s(const char* const str);
//void tty_write_sn(const char* const str, size_t len);
//void tty_write_hex(uint32_t hex);
//void tty_write_dec(uint32_t dec);


#ifdef __cplusplus
}
#endif

#endif
