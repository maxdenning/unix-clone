#ifndef _KERNEL_DESCRIPTOR_TABLES_H
#define _KERNEL_DESCRIPTOR_TABLES_H

#include <stdint.h>

namespace kernel::dt
{


// initialises global descriptor table and interrupt descriptor table
void dt_initialise(void);

// allows users to register isr interrupt handlers
void register_interrupt_handler(uint8_t interrupt, void(*handler)(const void* const));


}
#endif
