#ifndef _ARCH_I386_VGA_H
#define _ARCH_I386_VGA_H

#include <stdint.h>

namespace kernel::vga
{


// hardware text mode colour constants
enum VGAColour : uint8_t {
	COLOR_BLACK = 0,
	COLOR_BLUE = 1,
	COLOR_GREEN = 2,
	COLOR_CYAN = 3,
	COLOR_RED = 4,
	COLOR_MAGENTA = 5,
	COLOR_BROWN = 6,
	COLOR_LIGHT_GREY = 7,
	COLOR_DARK_GREY = 8,
	COLOR_LIGHT_BLUE = 9,
	COLOR_LIGHT_GREEN = 10,
	COLOR_LIGHT_CYAN = 11,
	COLOR_LIGHT_RED = 12,
	COLOR_LIGHT_MAGENTA = 13,
	COLOR_LIGHT_BROWN = 14,
	COLOR_WHITE = 15,
};

using VGAEntryColour = uint8_t;

static inline VGAEntryColour vga_entry_colour(VGAColour foreground, VGAColour background)
{
    return (VGAColour)(foreground | (background << 4U));
}

static inline uint16_t vga_entry(uint8_t uc, VGAEntryColour colour)
{
    return static_cast<uint16_t>(uc) | (static_cast<uint16_t>(colour) << 8);
}


}
#endif
