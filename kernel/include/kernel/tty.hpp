#ifndef _KERNEL_TTY_HPP
#define _KERNEL_TTY_HPP

#include <stddef.h>
#include <stdint.h>
#include "kernel/vga.hpp"

namespace kernel::tty
{


void tty_initialise(void);
void tty_clear(void);
void tty_set_colour(vga::VGAEntryColour col);
void tty_write(char c, vga::VGAEntryColour col = 0U);
void tty_write(const char* const str, size_t len, vga::VGAEntryColour col = 0U);
void tty_write(const char* const str, vga::VGAEntryColour col = 0U);
void tty_write_hex(uint32_t hex, vga::VGAEntryColour col = 0U);
void tty_write_dec(uint32_t dec, vga::VGAEntryColour col = 0U);


}
#endif
